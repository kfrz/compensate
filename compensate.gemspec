
lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'compensate/version'

Gem::Specification.new do |spec|
  spec.name          = 'compensate'
  spec.version       = Compensate::VERSION
  spec.authors       = ['Keifer Furzland']
  spec.email         = ['kfrz.code@gmail.com']

  spec.summary       = 'Calculate total reimbursement amount for a given CSV of project date ranges'
  spec.homepage      = 'https://gitlab.com/kfrz/compensate'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.executables << 'compensate'
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
