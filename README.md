# Compensate

This is a simple ruby gem used to calculate total reimbursement amount from a given CSV of project date ranges.

Completed as a technical exercise during interview process for [Simple Thread](https://simplethread.com)

## Installation

Install this gem directly with:

    $ gem install compensate

## Usage

Inputs accepted are .csv files with the following headers: `Rate, Start, End` where `Rate` is either `high` or `low`, and `Start, End` are dates in the format of `MM/DD/YY`. Ensure the header rows exist on a given CSV.

This gem is packaged with an executable command-line script. Use the following shell command to process a CSV named `test.csv`:

  `compensate test.csv`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/kfrz/compensate. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Compensate project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/kfrz/compensate/blob/master/CODE_OF_CONDUCT.md).
