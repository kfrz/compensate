RSpec.describe Compensate do
  it 'has a version number' do
    expect(Compensate::VERSION).not_to be nil
  end

  it 'correctly accumulates the recompensation amount' do
    set = Compensate::ProjectSet.new('spec/fixtures/test.csv')
    expect(set.total).to eq 'Total is $590.00'
  end

  it 'does not count duplicate days' do
    set = Compensate::ProjectSet.new('spec/fixtures/test2.csv')
    expect(set.total).to eq 'Total is $185.00'
  end
end
